# SMART DESK

## Разворачивание

### Project setup

```
npm install
```

#### Compiles and hot-reloads for development

```
npm run serve
```

#### Compiles and minifies for production

```
npm run build
```

## Заметки

- Дропдауна 2 штуки. Первый - `DropdownEmpty`, он крепится к чему угодно. Второй - `Dropdown`, это `DropdownEmpty`, который прикрепили к серому прямоугольнику :^).

- Логику дропдауна надо доделать.

  1. вытащить с помощью js на самый первый план (чтобы никакие position sticky его не перекрывали).
  2. Там выбирание в multiple вроде неверно работает.
  3. Надо сделать дропдаун в дропдауне.

- Vuex, VueRouter и всё такое не для верстки я в этот проект не добавляла.

- При разработке убрать `.gitlab-ci-yml`, это было, чтобы показать верстку на гитлабе. А еще во `vue.config.js` в `publicPath` убрать всё, потому что это настройки для гитлаба.
