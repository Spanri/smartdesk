const isProduction = process.env.NODE_ENV === "production";

module.exports = {
  root: true,
  env: {
    node: true
  },
  // extends: ["plugin:vue/essential", "@vue/prettier"],
  extends: ["plugin:vue/essential"],
  rules: {
    // Нельзя console.log в проде!
    "no-console": isProduction ? "error" : "off",
    "no-debugger": isProduction ? "error" : "off",
    "vue/no-parsing-error": ["error", { "x-invalid-end-tag": false }]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
