import Vue from "vue";
import App from "./App.vue";

/**
 * Для узнавания размеров блока(ов)
 */
import { VueResponsiveComponents } from "vue-responsive-components";
Vue.use(VueResponsiveComponents);

/**
 * Всплывашка при наведении на блок
 */
import VTooltip from "v-tooltip";
Vue.use(VTooltip);

/**
 * Директива, когда кликаешь вне блока, вызывается событие
 */
import vClickOutside from "v-click-outside";
Vue.use(vClickOutside);

import directives from "@/directives";
directives.forEach(directive => {
  Vue.use(directive);
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
