import Vue from "vue";

/**
 * Если прокручиваем блок, к которому это присоединено, блоки-родители не
 * прокручиваются (например, когда наш блок дошел до конца прокрутки, блок-родитель
 * может начать прокручиваться вниз)
 */

const map = new Map();

export default Vue.directive("prevent-parent-scroll", {
  bind(el) {
    const handler = event => {
      if (
        (el.scrollTop === 0 && event.deltaY < 0) ||
        (Math.abs(el.scrollTop - (el.scrollHeight - el.clientHeight)) <= 1 && event.deltaY > 0)
      ) {
        event.preventDefault();
      }
    };

    map.set(el, handler);
    el.addEventListener("wheel", handler);
  },

  unbind(el) {
    el.removeEventListener("wheel", map.get(el));
    map.delete(el);
  }
});
