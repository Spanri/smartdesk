module.exports = {
  runtimeCompiler: true,

  publicPath: process.env.NODE_ENV === "production" ? "/" + process.env.CI_PROJECT_NAME + "/" : "/",

  css: {
    loaderOptions: {
      sass: {
        prependData: `
					@import "./src/assets/scss/_base.scss";
				`
      }
    }
  }
};
